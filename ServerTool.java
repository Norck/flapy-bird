package flapyBird;

import java.net.SocketAddress;
import java.net.InetSocketAddress;
import java.net.InetAddress;
import java.util.Vector;

public class ServerTool{
	public static boolean isAddressDisponible(InetAddress address, Vector<InetAddress> vectInetAddress){
		boolean disponible = true;
		for(int i = 0; i < vectInetAddress.size(); i++){
			InetAddress tempIA = vectInetAddress.elementAt(i);
			if(tempIA.equals(address)){
				disponible = false;
				break;
			}
		}
		return disponible;
	}
	public static InetAddress getFreeGroupAddress(Vector<InetAddress> vectInetAddress)throws Exception{
		int octet1 = 0, octet2 = 0, octet3 = 0, octet4 = 0;
		InetAddress.getByName("255.255.255.0");
		for(octet1 = 224; octet1 < 240; octet1++){
			for(octet2 = 0; octet2 < 256; octet2++){
				for(octet3 = 0; octet3 < 256; octet3++){
					for(octet4 = 0; octet4 < 256; octet4++){
						InetAddress tempAddr = 
						InetAddress.getByName(octet1 + "." + octet2 + "." + octet3 + "." + octet4);
						boolean isDisponible = 
						isAddressDisponible(tempAddr, vectInetAddress);
						if(isDisponible){
							return tempAddr;
						}
					}
				}
			}
		}
		throw new Exception("Aucune addresse multicast n'est disponible.");
	}
	
	public static boolean isInTab(String[] tab, String word){
		boolean isIn = false;
		for(int i = 0; i < tab.length; i++){
			isIn = tab[i].equals(word);
			if(isIn){
				break;
			}
		}
		
		return isIn;
	}
	public static InetSocketAddress getInetSocketAddr(SocketAddress address){
		String rawAddress = address.toString();
		String[] addrWithoutSlash = rawAddress.split("/");
		String[] ipPort = addrWithoutSlash = addrWithoutSlash[1].split(":");
		InetSocketAddress addrToReturn = new InetSocketAddress(ipPort[0], Integer.parseInt(ipPort[1]));
		
		return addrToReturn;
	}
}
