package jeux;

import java.awt.Color;
import java.awt.Graphics;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author User
 */
public class Bird {
    public float x, y, vx, vy;
    public static final int RAD = 25;
    public Color col;
    public Bird(float width,float height,Color col) {
        x = width/2;
        y = height/2;
        this.col = col;
    }
    public void physics() {
        x+=vx;
        y+=vy;
        vy+=0.5f;
    }
    public void update(Graphics g) {
        g.setColor(col);
        g.fillRect((int)x,(int)y,20,20);
    }
    public void jump() {
        vy = -8;
    }
    
    public void reset() {
        x = 640/2;
        y = 640/2;
        vx = vy = 0;
    }
}
