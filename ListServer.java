package flapyBird;


import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import jeux.FlappyBird;

import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Container;

import java.net.InetSocketAddress;

import static javax.swing.GroupLayout.Alignment.CENTER;

public class ListServer extends JFrame {

    private DefaultListModel<String> model;
    private JList<String> myList;
    private JButton refreshListBtn;
    private JButton createServerBtn;
    public FlappyBird f;
    Player player;

    public ListServer() throws Exception{
		try{
			player = new Player();
			player.start();
			initUI();
		} catch(Exception e){
			throw(e);
		}
    }	

    private void createList() {

        model = new DefaultListModel<>();
        for(int i = 0; i < player.listPlayerServer.size(); i++){
			InetSocketAddress address = (InetSocketAddress) player.listPlayerServer.elementAt(i);
			String[] ip = address.getAddress().toString().split("/");
			model.addElement("ip:" + ip[1] + "port=" + address.getPort());
		}

        myList = new JList<>(model);
        myList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        myList.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {

                if (e.getClickCount() == 2) {

                    int index = myList.locationToIndex(e.getPoint());
					player.joinServer(index);
					f = new FlappyBird(player);
					f.go();
					

                }
            }
        });
    }

    private void createButtons() {

        refreshListBtn = new JButton("Refresh list");
        createServerBtn = new JButton("Create server");

        createServerBtn.addActionListener(e -> {
			try{
				player.initiateServer();
				//player.start();
			} catch(Exception e2){
				e2.printStackTrace();
			}
        });
        
         refreshListBtn.addActionListener(e -> {
			model.clear();
			try{
				player.getListServer();
				for(int i = 0; i < player.listPlayerServer.size(); i++){
					InetSocketAddress address = (InetSocketAddress) player.listPlayerServer.elementAt(i);
					String[] ip = address.getAddress().toString().split("/");
					model.addElement("ip:" + ip[1] + "port=" + address.getPort());
				}
			} catch(Exception e1){
				e1.printStackTrace();
			}
        });
    }

    private void initUI() {

        createList();
        createButtons();

        JScrollPane scrollPane = new JScrollPane(myList);
        createLayout(scrollPane, createServerBtn, refreshListBtn);

        setTitle("JList models");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private void createLayout(JComponent... arg) {

        Container pane = getContentPane();
        GroupLayout gl = new GroupLayout(pane);
        pane.setLayout(gl);

        gl.setAutoCreateContainerGaps(true);
        gl.setAutoCreateGaps(true);

        gl.setHorizontalGroup(gl.createSequentialGroup()
                .addComponent(arg[0])
                .addGroup(gl.createParallelGroup()
                        .addComponent(arg[1])
                        .addComponent(arg[2]))
        );

        gl.setVerticalGroup(gl.createParallelGroup(CENTER)
                .addComponent(arg[0])
                .addGroup(gl.createSequentialGroup()
                        .addComponent(arg[1])
                        .addComponent(arg[2]))
        );

        gl.linkSize(createServerBtn, refreshListBtn);

        pack();
    }
    
    public static void main(String[] args){
			
		EventQueue.invokeLater(() -> {
			try{
				ListServer ex = new ListServer();
				ex.setVisible(true);
			} catch(Exception e){
			e.printStackTrace();
			}
		});
	}

}
