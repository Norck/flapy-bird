package jeux;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

import flapyBird.Player;

public class FlappyBird implements ActionListener, KeyListener {
    
    public static final int FPS = 60, WIDTH = 640, HEIGHT = 480;
    private Bird bird;
    public Bird enemie;
    private JFrame frame;
    private JPanel panel;
    private ArrayList<Rectangle> rects;
    private int time, scroll;
    private Timer t;
    public Player p;
    
    private boolean paused;
    public FlappyBird(Player p) {
    	this.p=p;
    }
    public void go() {
        frame = new JFrame("Flappy Bird");
        bird = new Bird((float)WIDTH,(float)HEIGHT,Color.pink);
        enemie = new Bird((float)WIDTH,(float)HEIGHT,Color.MAGENTA);
        rects = new ArrayList<Rectangle>();
        panel = new GamePanel(this, bird,enemie, rects);
        frame.add(panel);
        
        frame.setSize(WIDTH, HEIGHT);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.addKeyListener(this);
        
        paused = true;
        
        t = new Timer(1000/FPS, this);
        t.start();
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        panel.repaint();
        if(!paused) {
            bird.physics();
            enemie.physics();
            if(scroll % 90 == 0) {
                Rectangle r = new Rectangle(WIDTH, 0, GamePanel.PIPE_W, (int) ((HEIGHT/2)/5f + (0.2f)*HEIGHT));
                int h2 = (int) ((HEIGHT/2)/5f + (0.2f)*HEIGHT);
                Rectangle r2 = new Rectangle(WIDTH, HEIGHT - h2, GamePanel.PIPE_W, h2);
                rects.add(r);
                rects.add(r2);
            }
            ArrayList<Rectangle> toRemove = new ArrayList<Rectangle>();
            boolean game = true;
            for(Rectangle r : rects) {
                r.x-=3; 
                if(r.x + r.width <= 0) {
                    toRemove.add(r);
                }
                if(r.contains(bird.x, bird.y)) {
                    JOptionPane.showMessageDialog(frame, "You lose!\n"+"Your score was: "+time+".");
                    game = false;
                }
                else if(r.contains(enemie.x, enemie.y)) {
                    JOptionPane.showMessageDialog(frame, "You win!\n"+"Your score was: "+time+".");
                    game = false;
                }
            }
            rects.removeAll(toRemove);
            time++;
            scroll++;

            if(bird.y > HEIGHT || bird.y+bird.RAD < 0) {
            	JOptionPane.showMessageDialog(frame, "You lose!\n"+"Your score was: "+time+".");
            	game = false;
            }
            if(enemie.y > HEIGHT || bird.y+bird.RAD < 0) {
            	JOptionPane.showMessageDialog(frame, "You win!\n"+"Your score was: "+time+".");
            	game = false;
            }

            if(!game) {
                rects.clear();
                bird.reset();
                enemie.reset();
                time = 0;
                scroll = 0;
                paused = true;
            }
        }
        else {
            
        }
    }
    
    public int getScore() {
        return time/100;
    }
    
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode()==KeyEvent.VK_UP) {
            bird.jump();
            try {
				p.sendToGroup("up", 1);
			} catch (Exception e1) {
				e1.printStackTrace();
			};
        }
        if(e.getKeyCode()==KeyEvent.VK_DOWN) {
            enemie.jump();
        }
        else if(e.getKeyCode()==KeyEvent.VK_SPACE) {
            paused = false;
        }
    }
    public void keyReleased(KeyEvent e) {
        
    }
    public void keyTyped(KeyEvent e) {
        
    }
    
    public boolean paused() {
        return paused;
    }
}
