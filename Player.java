package flapyBird;
//import flapyBird.ListServer;
//import jeux.FlappyBird;

import java.net.DatagramSocket;	
import java.net.SocketAddress;
import java.net.InetSocketAddress;
import java.net.DatagramPacket;
import java.net.SocketException;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.NetworkInterface;
import java.util.Scanner;  
import java.lang.Thread;
import java.util.Vector;
import java.nio.charset.Charset;
import java.lang.reflect.Method;
import java.lang.Thread;

public class Player extends Thread{
	public DatagramSocket playerSocket;// sert d'identifiant unique
	public MulticastSocket[] tabGroup;
	public InetAddress[] tabGroupAddress;
	public boolean isServer;
	public Vector<InetSocketAddress> listPlayerServer; // list des serveurs obtenu apres demande au group
	public Vector<InetAddress> multicastAddrUsed;
	
	public void printPlayerDetails(){
		System.out.println("playerSocket: " + playerSocket.getLocalSocketAddress().toString());
		printListPlayerServer();
		printGroupAddress();
	}
	
	public void printListPlayerServer(){
		System.out.println("--------------SERVER  LIST--------------");
		for(int i = 0; i < listPlayerServer.size(); i++){
			InetSocketAddress address = (InetSocketAddress) listPlayerServer.elementAt(i);
			System.out.println("player" + i + ":" + address.getAddress() + " " + address.getPort());
		}
		System.out.println("--------------------------------------------");
	}
	public void printGroupAddress(){
		System.out.println("--------------GROUP ADDRESS--------------");
		for(int i = 0; i < tabGroupAddress.length; i++){
			if(tabGroup[i] != null){
				System.out.println("ip=" + tabGroupAddress[i].toString() + " port=" + tabGroup[i].getLocalPort());
			}
		}
		System.out.println("--------------------------------------------");
	}
	
	public Player()throws Exception{
		try{
			playerSocket = new DatagramSocket();
			playerSocket.setSoTimeout(100);
			tabGroup = new MulticastSocket[2];
			tabGroup[0] = new MulticastSocket(1111);
			tabGroup[0].setSoTimeout(100);
			tabGroup[1] = null;
			tabGroupAddress = new InetAddress[2];
			tabGroupAddress[0] = InetAddress.getByName("224.0.0.1");
			tabGroup[0].setSoTimeout(100);
			tabGroupAddress[1] = null;
			tabGroup[0].joinGroup(tabGroupAddress[0]);
			listPlayerServer = new Vector<InetSocketAddress>(5,5);
			System.out.println("listServer = " + listPlayerServer.size());
			multicastAddrUsed = new Vector<InetAddress>(5,5);
			multicastAddrUsed.add( InetAddress.getByName("224.0.0.0") );
			multicastAddrUsed.add( tabGroupAddress[0] );
			isServer = false;
			printPlayerDetails();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void closeSocket()throws Exception{
		try{
			playerSocket.close();
			for(int i = 0; i < tabGroup.length; i++){
				if(tabGroup[i] != null){
					tabGroup[i].close();
				}
			}
		} catch(Exception e){
			throw(e);
		}
	}
	
	
	public void sendToGroup(String message, int indexGroup)throws Exception{
		try{
			byte[] byteMsg = message.getBytes();
			DatagramPacket packet = new DatagramPacket(byteMsg, byteMsg.length);
			if((indexGroup < 0) || (indexGroup  > 1)){
				throw new Exception("index de groupe doit etre 1 ou 0");
			}
			packet.setAddress(tabGroupAddress[0]);
			packet.setPort(1111);
			playerSocket.send(packet);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void sendMessage(String message, InetSocketAddress address)throws Exception{
		try{
			byte[] buf = message.getBytes();
			DatagramPacket packet = new DatagramPacket(buf, buf.length);
			packet.setAddress(address.getAddress());
			packet.setPort(address.getPort());
			playerSocket.send(packet);
		} catch(Exception e){
			throw(e);
		}
	}
	
	public void sendToPlayerServer(String message, int indexPlayer)throws Exception{
		try{
			byte[] byteMsg = message.getBytes();
			DatagramPacket packet = new DatagramPacket(byteMsg, byteMsg.length);
			InetSocketAddress server = (InetSocketAddress) listPlayerServer.elementAt(indexPlayer);
			packet.setAddress(server.getAddress());
			packet.setPort(server.getPort());
			playerSocket.send(packet);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public DatagramPacket receiveMessage(int anyInt)throws Exception{
		DatagramPacket packet = null;
		try{
			byte[] buf = new byte[1000];
			DatagramPacket recv = new DatagramPacket(buf, buf.length);
			String msg = new String(recv.getData(), recv.getOffset(), recv.getLength());
			tabGroup[1].receive(recv);
			msg = new String(recv.getData(), recv.getOffset(), recv.getLength());
			if(!msg.equals("")){
				packet = recv;
				System.out.println("messageToGroup[1] = " + msg);
				return packet;
			}
		} catch(Exception e3){
			//throw(e3);
		}
		return packet;
	}
	
	public DatagramPacket receiveMessage()throws Exception{
		DatagramPacket packet = null;
		try{
			byte[] buf = new byte[1000];
			DatagramPacket recv = new DatagramPacket(buf, buf.length);
			playerSocket.receive(recv);
			String msg = new String(recv.getData(), recv.getOffset(), recv.getLength());
			if(!msg.equals("")){
				packet = recv;
				System.out.println("messagetoPlayerSocket = " + msg);
				return packet;
			}
		}
		catch(Exception e){
			try{
				byte[] buf = new byte[1000];
				DatagramPacket recv = new DatagramPacket(buf, buf.length);
				String msg = new String(recv.getData(), recv.getOffset(), recv.getLength());
				tabGroup[0].receive(recv);
				msg = new String(recv.getData(), recv.getOffset(), recv.getLength());
				if(!msg.equals("")){
					packet = recv;
					System.out.println("messageToGroup[0] = " + msg);
					return packet;
				}
			} catch(Exception e2){
				try{
					byte[] buf = new byte[1000];
					DatagramPacket recv = new DatagramPacket(buf, buf.length);
					String msg = new String(recv.getData(), recv.getOffset(), recv.getLength());
					tabGroup[1].receive(recv);
					msg = new String(recv.getData(), recv.getOffset(), recv.getLength());
					if(!msg.equals("")){
						packet = recv;
						System.out.println("messageToGroup[1] = " + msg);
						return packet;
					}
				} catch(Exception e3){
					//throw(e3);
				}
			}
		}
		return packet;
	}
	
	public void getUsedGrpAddr(String address)throws Exception{
		try{
			multicastAddrUsed.add(InetAddress.getByName(address));
		} catch(Exception e){
			throw(e);
		}
	}
	
	public int callModifCommands(DatagramPacket messagePacket)throws Exception{
	//ne fait rien si le message n'est pas une commande
		try{
			String[] listCommands = new String[]{ "getUsedGrpAddr","getServer","getGroupAddress"};
			
			byte[] msgByte = new byte[messagePacket.getLength()];
            System.arraycopy(messagePacket.getData(), messagePacket.getOffset(), msgByte, 0, messagePacket.getLength());
			
			String msg = new String(msgByte);
			String[] message = msg.split(" ");
			boolean isCommandName = ServerTool.isInTab(listCommands, message[0]);
			if(isCommandName == true){
				Class playerClass = Class.forName("flapyBird.Player");
				Method method = playerClass.getMethod(message[0], String.class);
				if(method == null){
					throw new Exception("La commande '" + message[0] + "' est introuvable.");
				}
				String[] arg = msg.split(message[0].concat(" "));
				System.out.println("m2 = " + arg[1]);
				method.invoke(this, arg[1]);
				return 1;
			}
		} catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}
	
	public int sendInfoToPlayer(DatagramPacket messagePacket)throws Exception{
	//ne fait rien si le message n'est pas une commande
		try{
			String[] listCommands = new String[]{"sendServer", "sendGroupAddress"};
			byte[] msgByte = new byte[messagePacket.getLength()];
            System.arraycopy(messagePacket.getData(), messagePacket.getOffset(), msgByte, 0, messagePacket.getLength());
			String[] message = new String(msgByte).split(" ");
			boolean isCommandName = ServerTool.isInTab(listCommands, message[0]);
			if(isCommandName == true){
				Class playerClass = Class.forName("flapyBird.Player");
				Method method = playerClass.getMethod(message[0], InetSocketAddress.class);
				if(method == null){
					throw new Exception("La commande '" + message[0] + "' est introuvable.");
				}
				InetSocketAddress requesterAddress = 
				ServerTool.getInetSocketAddr(messagePacket.getSocketAddress());
				method.invoke(this, requesterAddress);
				return 1;
			}
		} catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}
	
	public void callCommands(DatagramPacket messagePacket)throws Exception{
		try{
			if(callModifCommands(messagePacket) == 0){
				sendInfoToPlayer(messagePacket);
			}
		} catch(Exception e){
			throw(e);
		}
	}
	
	public void sendGroupAddress(InetSocketAddress requester)throws Exception{
	//renvoie l'addresse de groupe cree par le serveur
		try{
			if(tabGroupAddress[1] != null){
				String ip[] = tabGroupAddress[1].toString().split("/");
				int port = tabGroup[1].getLocalPort();
				String message = "getGroupAddress " + ip[1] + " " + port;
				sendMessage(message, requester);
			}
		} catch(Exception e){
			throw(e);
		}	
	}
	
	public void getGroupAddress(String arg)throws Exception{
		try{
			String[] ipPort = arg.split(" ");
			MulticastSocket group = new MulticastSocket(Integer.parseInt(ipPort[1]));
			tabGroup[1] = group;
			tabGroup[1].setSoTimeout(100);
			InetAddress groupAddress = InetAddress.getByName(ipPort[0]);
			tabGroupAddress[1] = groupAddress;
			
			printPlayerDetails();
		} catch(Exception e){
			throw(e);
		}
	}
	
	public void sendServer(InetSocketAddress requester)throws Exception{
	//renvoie l'addresse du player serveur
		try{
			if(isServer == true){
				String[] ip = playerSocket.getLocalAddress().toString().split("/");
				int port = playerSocket.getLocalPort();
				String message = "getServer " +  ip[1] + " " + port;
				System.out.println(message);
				sendMessage(message, requester);
				
			}
		} catch(Exception e){
			
		}
	}
	
	public void getServer(String arg)throws Exception{
		try{
			String[] ipPort = arg.split(" ");
			InetSocketAddress serverAddress = new InetSocketAddress(ipPort[0], Integer.parseInt(ipPort[1]));
			listPlayerServer.add(serverAddress);
			printPlayerDetails();
		} catch(Exception e){
			throw(e);
		}
	}
	
	public void initiateServer()throws Exception{
		try{
			InetAddress groupAddress = ServerTool.getFreeGroupAddress(multicastAddrUsed);
			String[] ip = groupAddress.toString().split("/");
			MulticastSocket tempGroup = new MulticastSocket(0);
			tabGroup[1] = tempGroup;
			tabGroup[1].setSoTimeout(100);
			tabGroupAddress[1] = groupAddress;
			this.isServer = true;
			
			System.out.println("------server created-------");
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void getListServer() throws Exception{
		try{
			sendToGroup("sendServer", 0);
			DatagramPacket messagePacket = null;
			listPlayerServer.clear();
			int compteur = 0;
			System.out.println("Searching servers..."); 
			while(compteur < 4){
				compteur++;
				messagePacket = receiveMessage();
				if(messagePacket!= null){
					String msg = new String(messagePacket.getData(), messagePacket.getOffset(), messagePacket.getLength());
					String[] command  = msg.split(" ");
					System.out.println("command = '" + command[0] + "'");
					if(!command[0].equals("getServer")){
						continue;
					}
					else{
						System.out.println("Serveur trouve.");
						InetSocketAddress senderAddress = (InetSocketAddress) messagePacket.getSocketAddress();
						listPlayerServer.add(senderAddress);
					}
				}
			}
			printPlayerDetails();
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void joinServer(int indexServer){
		try{
			try{
				InetSocketAddress server = (InetSocketAddress) listPlayerServer.elementAt(indexServer);
				sendMessage("sendGroupAddress", server);
			}
			catch(java.net.SocketTimeoutException ste){
				throw new Exception("Impossible de joindre le serveur.");
			}
			try{
				DatagramPacket receivedPacket = receiveMessage();
				callCommands(receivedPacket);
			}
			catch(java.net.SocketTimeoutException ste2){
				throw new Exception("Impossible d'obtenir l'addresse de groupe.");
			}
			
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	//public void getServerList(){
		//sendToGroup("get");
	//}
	
	public static boolean isEmptyMessage(byte[] message){
		boolean empty = true;
		for(int i = 0; i < message.length; i++){
			if(message[i] == 1){
				return false;
			}
		}
		return empty;
	}
	
	public void run(){
		try{
			//System.out.println("default Charset = " + Charset.defaultCharset().displayName());
			//sendToGroup("sendServer",0);
			Thread receive = new Thread("messageReceiver"){
				public void run(){
					DatagramPacket messagePacket = null;
					try{
						while(true){
							messagePacket = receiveMessage();
							if(messagePacket != null){
								byte[] msgByte = new byte[messagePacket.getLength()];
					            System.arraycopy(messagePacket.getData(), messagePacket.getOffset(), msgByte, 0, messagePacket.getLength());
								String message = new String(msgByte);
								String[] command = message.split(" ");
								if(command[0] == "beginParty") {
									break;
								}
								callCommands(messagePacket);
							}
							Thread.sleep(50);
						}
					} catch(Exception e){

					}
				}
			};
			receive.setDaemon(true);
			receive.start();
			Thread party = new Thread("partyMessage"){
				public void run(){
					DatagramPacket messagePacket = null;
					try{
						while(true){
							messagePacket = receiveMessage();
							if(messagePacket != null){
								byte[] msgByte = new byte[messagePacket.getLength()];
					           System.out.println(new String(msgByte));
							}
							Thread.sleep(20);
						}
					} catch(Exception e){

					}
				}
			};
			party.setDaemon(true);
			party.start();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}	
	public static void main(String[] args)throws Exception{
		try{
			 Player player = new Player();
			 player.initiateServer();
			 player.run();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}
